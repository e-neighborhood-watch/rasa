port module Main exposing (main)

import Browser
import Dict exposing (Dict)
import Html.Styled as Html
import Json.Decode as D
import View exposing
  ( view
  , pageLocDecoder
  )
import Update exposing
  ( update
  )
import Subscriptions exposing
  ( subscriptions
  )
import Types exposing (..)

port requestNodeSize : Id -> Cmd msg
port receiveNodeSize : (D.Value -> msg) -> Sub msg

initialModel : Model
initialModel =
  { nodes =
    Dict.empty
  , lastMouse =
    Nothing
  , selectedNode =
    Nothing
  , nextId =
    0
  , textFieldContents =
    ""
  }

main : Program Flags Model Mesgs
main =
  Browser.document
    { init = \ () -> ( initialModel, Cmd.none )
    , view = \ model ->
      { title = "RASA"
      , body = model |> view |> List.map Html.toUnstyled
      }
    , update = update requestNodeSize
    , subscriptions = subscriptions receiveNodeSize
    }
