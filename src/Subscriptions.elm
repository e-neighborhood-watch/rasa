module Subscriptions exposing
  ( subscriptions
  )

import Browser.Events as BrowserEvent
import Json.Decode as D
import Types exposing (..)
import View exposing (pageLocDecoder)

decodeReceiveSize : D.Decoder (Result String (Size { nodeId : Id }))
decodeReceiveSize =
  D.oneOf
    [ D.map3
        (\ id w h -> Ok { w = w, h = h , nodeId = id })
        (D.field "nodeId" D.int)
        (D.float |> D.field "width" |> D.maybe |> D.field "rect" |> D.map (Maybe.withDefault 0))
        (D.float |> D.field "height" |> D.maybe |> D.field "rect" |> D.map (Maybe.withDefault 0))
    , D.string |> D.field "error" |> D.map Err
    ]

subscriptions : ((D.Value -> Mesgs) -> Sub Mesgs) -> Model -> Sub Mesgs
subscriptions receiveNodeSize model =
  Sub.batch
    ( [ D.decodeValue decodeReceiveSize
        >> Result.mapError ReceiveSizeDecodeError
        >> Result.andThen (Result.mapError ReceiveSizeExecuteError)
        >> ReceiveSize
        |> receiveNodeSize
      ]
      ++
      case model.lastMouse of
        Nothing ->
          []
        Just _ ->
          [ Move |> pageLocDecoder |> BrowserEvent.onMouseMove
          , UnClick |> pageLocDecoder |> BrowserEvent.onMouseUp
          ]
    )
