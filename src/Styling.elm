module Styling exposing
  ( darkMode
  , connection
  , nodeText
  , nodeRect
  , svg
  , form
  , textField
  )

import Css exposing (..)
import Types exposing (Size)

type alias ColorScheme a b c d e f g =
  { outline
    : ColorValue a
  , focusedOutline
    : ColorValue b
  , background
    : ColorValue c
  , textFieldBackground
    : ColorValue d
  , nodeRounding
    : LengthOrNumber { e | value : String }
  , textFieldRounding
    : Length f g
  , padding
    : Float
  }

darkMode =
  { outline =
    hex "#BB44AA"
  , focusedOutline =
    hex "#44BBAA"
  , background =
    hex "#333333"
  , textFieldBackground =
    hex "#3F3F3F"
  , nodeRounding =
    num 10
  , textFieldRounding =
    px 5
  , padding =
    20
  }

noUserSelect : Style
noUserSelect =
  batch
    [ property "-moz-user-select" "none"
    , property "user-select" "none"
    ]

svgStroke : ColorValue a -> Style
svgStroke color = property "stroke" color.value

connection : ColorScheme a b c d e f g -> List Style
connection { outline } =
  [ svgStroke outline
  , fill transparent
  ]

nodeText : ColorScheme a b c d e f g -> Bool -> List Style
nodeText { outline, focusedOutline } focused =
  [ property "dominant-baseline" "central"
  , property "text-anchor" "middle"
  , noUserSelect
  , if
      focused
    then
      fill focusedOutline
    else
      fill outline
  ]

nodeRect : ColorScheme a b c d e f g -> Bool -> List Style
nodeRect { background, outline, focusedOutline } focused =
  [ fill background
  , if
      focused
    then
      svgStroke focusedOutline
    else
      svgStroke outline
  ]

topWidthSpanning : Style
topWidthSpanning =
  Css.batch
    [ position fixed
    , top zero
    , left zero
    , width <| pct 100
    ]

svg : ColorScheme a b c d e f g -> List Style
svg { background } =
  [ topWidthSpanning
  , height <| pct 100
  , backgroundColor background
  ]

form : List Style
form =
  [ topWidthSpanning
  , displayFlex
  , flexDirection column
  ]

textField : ColorScheme a b c d e f g -> List Style
textField { textFieldBackground, outline, focusedOutline, textFieldRounding } =
  [ backgroundColor textFieldBackground
  , color outline
  , borderColor outline
  , Css.outline none
  , borderRadius textFieldRounding
  , borderStyle solid
  , fontSize <| px 16
  , fontFamily monospace
  , margin <| px 5
  , focus
      [ color focusedOutline
      , borderColor focusedOutline
      ]
  ]
