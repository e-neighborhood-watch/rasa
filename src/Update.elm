module Update exposing
  ( update
  )

import Browser.Dom as Dom
import Dict exposing (Dict)
import Set exposing (Set)
import Styling
import Task
import Types exposing (..)

addPos : Pos a -> Pos b -> Pos b
addPos {x, y} pos2 =
    { pos2
    | x =
      x + pos2.x
    , y =
      y + pos2.y
    }

subPos : Pos a -> Pos b -> Pos b
subPos {x, y} pos2 =
  { pos2
  | x =
    x - pos2.x
  , y =
    y - pos2.y
  }

moveNodesBy : MouseTarget -> Pos a -> Dict Int Node -> Dict Int Node
moveNodesBy moveTarget diff =
  case moveTarget of
    Background ->
      Dict.map
        ( \ _ -> addPos diff )
    Node nodeId ->
      Dict.update
        nodeId
        (Maybe.map (addPos diff))

blurTextField : Cmd Mesgs
blurTextField = "text-field" |> Dom.blur |> Task.attempt TextFieldBlurred

updateNodeText : String -> Maybe Node -> Maybe Node
updateNodeText nodeText maybeNode =
  case maybeNode of
    Nothing ->
      Just
        { x = 0
        , y = 0
        , size = NotReady
        , text = String.trim nodeText
        , premises = Set.empty
        }
    Just node ->
      Just
        { node
        | text = String.trim nodeText
        }

deleteNode : Id -> Dict Id Node -> Dict Id Node
deleteNode toDelete nodes =
  nodes
  |> Dict.remove toDelete
  |> Dict.map ( \ _ node -> { node | premises = Set.remove toDelete node.premises } )

update : (Id -> Cmd Mesgs) -> Mesgs -> Model -> (Model, Cmd Mesgs)
update requestNodeSize mesg model =
  case mesg of
    Click moveTarget { altKey, pagePosition } ->
      if
        altKey
      then
        ( case model.selectedNode of
            Nothing ->
              model
            Just conclusionNodeId ->
              case moveTarget of
                Background ->
                  model
                Node premiseNodeId ->
                  if
                    Dict.member conclusionNodeId model.nodes |> not
                  then
                    model
                  else
                    { model
                    | nodes =
                        Dict.update
                          conclusionNodeId
                          ( Maybe.map <|
                              \ node -> 
                                { node
                                | premises =
                                    if
                                      Set.member premiseNodeId node.premises
                                    then
                                      Set.remove premiseNodeId node.premises
                                    else
                                      Set.insert premiseNodeId node.premises
                                }
                          )
                          model.nodes
                    }
        , Cmd.none
        )
      else
        ( { model
          | nodes =
              case model.selectedNode of
                Nothing ->
                  model.nodes
                Just nodeId ->
                  if
                    moveTarget == Node nodeId || (model.textFieldContents |> String.trim |> String.isEmpty)
                  then
                    model.nodes
                  else
                    Dict.update
                      nodeId
                      (updateNodeText model.textFieldContents)
                      model.nodes
          , lastMouse =
              Just
                { x = pagePosition.x
                , y = pagePosition.y
                , moveTarget = moveTarget
                }
          , selectedNode =
              case moveTarget of
                Background -> Nothing
                Node id -> Just id
          , textFieldContents =
              case moveTarget of
                Background -> ""
                Node id ->
                  if
                    model.selectedNode == Just id
                  then
                    model.textFieldContents
                  else
                    model.nodes |> Dict.get id |> Maybe.map .text |> Maybe.withDefault ""
          }
        , case model.selectedNode of
            Nothing ->
              Cmd.none
            Just nodeId ->
              if
                moveTarget == Node nodeId || not (model.textFieldContents |> String.trim |> String.isEmpty)
              then
                Cmd.none
              else
                nodeId |> requestNodeSize
        )
    Move loc ->
      ( case model.lastMouse of
          Nothing ->
            model
          Just moveInfo ->
            { model
            | nodes =
              moveNodesBy moveInfo.moveTarget (subPos loc moveInfo) model.nodes
            , lastMouse =
              Just
                { x =
                  loc.x
                , y =
                  loc.y
                , moveTarget =
                  moveInfo.moveTarget
                }
            }
      , Cmd.none
      )
    UnClick loc ->
      ( case model.lastMouse of
          Nothing ->
            model
          Just moveInfo ->
            { model
            | nodes =
              moveNodesBy moveInfo.moveTarget (subPos loc moveInfo) model.nodes
            , lastMouse =
              Nothing
            }
      , Cmd.none
      )
    RemoveNode id ->
      ( { model
        | nodes =
            deleteNode
              id
              model.nodes
        , selectedNode =
            if
              model.selectedNode == Just id
            then
              Nothing
            else
              model.selectedNode
        , textFieldContents =
            if
              model.selectedNode == Just id
            then
              ""
            else
              model.textFieldContents
        }
      , Cmd.none
      )
    TextChanged newText ->
      ( { model
        | textFieldContents = newText
        }
      , case model.selectedNode of
          Nothing ->
            Cmd.none
          Just nodeId ->
            nodeId |> requestNodeSize
      )
    TextConfirmed ->
      if
        model.textFieldContents |> String.trim |> String.isEmpty
      then
        ( { model | selectedNode = Nothing }
        , Cmd.batch
            [ blurTextField
            , case model.selectedNode of
                Nothing ->
                  Cmd.none
                Just nodeId ->
                  nodeId |> requestNodeSize
            ]
        )
      else
        case model.selectedNode of
          Nothing ->
            if
              Dict.member model.nextId model.nodes
            then
              ( { model | nextId = model.nextId + 1 }, Cmd.none )
            else
              ( { model
                | nodes =
                    Dict.insert
                      model.nextId
                      { x = 0
                      , y = 0
                      , size = NotReady
                      , text = model.textFieldContents
                      , premises = Set.empty
                      }
                      model.nodes
                , nextId = model.nextId + 1
                , textFieldContents = ""
                }
              , Cmd.batch
                  [ model.nextId |> requestNodeSize
                  , blurTextField
                  ]
              )
          Just id ->
            ( { model
              | nodes =
                  Dict.update
                  id
                  (updateNodeText model.textFieldContents)
                  model.nodes
              , textFieldContents = ""
              , selectedNode = Nothing
              }
            , Cmd.batch
                [ requestNodeSize id
                , blurTextField
                ]
            )
    ReceiveSize result ->
      case result of
        Ok { w, h, nodeId } ->
          ( { model
            | nodes =
                Dict.update
                  nodeId
                  ( Maybe.map
                      ( \ node ->
                          case node.size of
                            Ready _ ->
                              { node
                              | size = Ready { w = w, h = h }
                              }
                            NotReady ->
                              { node
                              | size = Ready { w = w, h = h }
                              , x = w / 2 + Styling.darkMode.padding
                              , y = h / 2 + Styling.darkMode.padding
                              }
                      )
                  )
                  model.nodes
            }
          , Cmd.none
          )
        Err err ->
          ( model, Cmd.none )
    TextFieldBlurred result ->
      ( model, Cmd.none )

