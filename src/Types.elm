module Types exposing
  ( Pos
  , Size
  , Id
  , Status (..)
  , Flags
  , Node
  , Model
  , ClickData
  , MouseTarget (..)
  , Mesgs (..)
  , ReceiveSizeError (..)
  )

import Browser.Dom as Dom
import Dict exposing (Dict)
import Json.Decode as D
import Set exposing (Set)

type alias Pos a =
  { a
  | x : Float
  , y : Float
  }

type alias Size a =
  { a
  | w : Float
  , h : Float
  }

type alias Id = Int

type Status a
  = Ready a
  | NotReady

type alias Flags = ()

type alias Node =
  Pos
    { size : Status (Size {})
    , text : String
    , premises : Set Id
    }

type alias Model =
  { nodes
    : Dict Id Node
  , lastMouse
    : Maybe
      ( Pos
        { moveTarget
          : MouseTarget
        }
      )
  , selectedNode
    : Maybe Id
  , nextId
    : Id
  , textFieldContents
    : String
  }

type alias ClickData =
  { pagePosition : Pos {}
  , altKey : Bool
  }

type MouseTarget
  = Node Id
  | Background

type ReceiveSizeError
  = ReceiveSizeDecodeError D.Error
  | ReceiveSizeExecuteError String

type Mesgs
  = Click MouseTarget ClickData
  | RemoveNode Id
  | Move (Pos {})
  | UnClick (Pos {})
  | ReceiveSize (Result ReceiveSizeError (Size { nodeId : Id }))
  | TextChanged String
  | TextConfirmed
  | TextFieldBlurred (Result Dom.Error ())
