module View exposing
  ( view
  , pageLocDecoder
  )

import Browser
import Browser.Events as BrowserEvent
import Dict exposing (Dict)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as HtmlAttribute
import Html.Styled.Events as HtmlEvent
import Json.Decode as D
import Set exposing (Set)
import Styling exposing (darkMode)
import Svg.Styled as Svg exposing (Svg, svg)
import Svg.Styled.Attributes as SvgAttribute
import Svg.Styled.Events as SvgEvent
import Svg.Styled.Keyed as SvgKeyed
import Types exposing (..)

viewConnections : Dict Id Node -> List ( String, Svg msgs )
viewConnections nodes =
  let
    viewConnection : Node -> Node -> Maybe (Svg msgs)
    viewConnection consequence premise =
      case consequence.size of
        NotReady ->
          Nothing
        Ready consequenceSize ->
          case premise.size of
            NotReady ->
              Nothing
            Ready premiseSize ->
              let
                a : Float
                a =
                  0
                b : Float
                b =
                  1
                c : Float
                c =
                  0.25
                premisePoint : Pos {}
                premisePoint =
                  { x = premise.x
                  , y = premise.y + (premiseSize.h / 2 + Styling.darkMode.padding)
                  }
                consequencePoint : Pos {}
                consequencePoint =
                  { x = consequence.x
                  , y = consequence.y - (consequenceSize.h / 2 + Styling.darkMode.padding)
                  }
                midpoint : Pos {}
                midpoint =
                  { x = ( consequence.x + premise.x ) / 2
                  , y = ( consequence.y + premise.y ) / 2
                  }
              in
                Just <|
                  Svg.path
                    [ Styling.connection Styling.darkMode |> SvgAttribute.css
                    , SvgAttribute.d <|
                        String.concat
                          [ "M"
                          , String.fromFloat consequencePoint.x
                          , ","
                          , String.fromFloat consequencePoint.y
                          , "C"
                          , String.fromFloat consequencePoint.x
                          , ","
                          , String.fromFloat <| consequencePoint.y - a / 2
                          , " "
                          , String.fromFloat <| midpoint.x + b * (consequencePoint.x - premisePoint.x) / 2
                          , ","
                          , String.fromFloat <| midpoint.y + c * (consequencePoint.y - premisePoint.y) / 2
                          , " "
                          , String.fromFloat midpoint.x
                          , ","
                          , String.fromFloat midpoint.y
                          , "S"
                          , String.fromFloat premisePoint.x
                          , ","
                          , String.fromFloat <| premisePoint.y + a / 2
                          , " "
                          , String.fromFloat premisePoint.x
                          , ","
                          , String.fromFloat premisePoint.y
                          ]
                    ]
                    []
  in
    nodes
    |> Dict.toList
    |> List.concatMap
        ( \ ( nodeId, node ) ->
            case node.size of
              NotReady ->
                []
              Ready _ ->
                node.premises
                |> Set.toList
                |> List.filterMap
                  ( \ premiseId ->
                      nodes
                      |> Dict.get premiseId
                      |> Maybe.andThen (viewConnection node)
                      |> Maybe.map
                          ( \ element ->
                              ( String.fromInt premiseId ++ "," ++ String.fromInt nodeId
                              , element
                              )
                          )
                  )
        )

pageLocDecoder : (Pos {} -> msg) -> D.Decoder msg
pageLocDecoder f =
  D.map2
    ( \ x y ->
        f
          { x = x
          , y = y
          }
    )
    (D.field "pageX" D.float)
    (D.field "pageY" D.float)


mouseEventDecoder : (ClickData -> msg) -> D.Decoder msg
mouseEventDecoder f =
  D.map2
    ( \ pos altKey ->
        f
          { pagePosition = pos
          , altKey = altKey
          }
    )
    (pageLocDecoder identity)
    (D.field "altKey" D.bool)

view : Model -> List (Html Mesgs)
view model =
  let
    focused : Id -> Bool
    focused id = model.selectedNode == Just id

    textNode : Id -> Node -> Svg Mesgs
    textNode nodeId node =
      Svg.text_
        [ "node_" ++ String.fromInt nodeId |> SvgAttribute.id
        , node.x |> String.fromFloat |> SvgAttribute.x
        , node.y |> String.fromFloat |> SvgAttribute.y
        , focused nodeId |> Styling.nodeText Styling.darkMode |> SvgAttribute.css
        ]
        [ Svg.text <|
            if
              focused nodeId
            then
              String.trim model.textFieldContents
            else
              node.text
        ]
    viewNode : (Id, Node) -> (String, Svg Mesgs)
    viewNode (id, node) =
      ( String.fromInt id
      , case node.size of
          Ready size ->
            Svg.g
              ( ( case model.lastMouse of
                    Nothing ->
                      [ id |> Node |> Click |> mouseEventDecoder |> D.map (\ x -> (x, True)) |> SvgEvent.stopPropagationOn "mousedown"
                      ]
                    Just selId -> []
                )
                ++
                -- We need stop propagation here to prevent us triggering adding a node as well
                [ (RemoveNode id, True) |> D.succeed |> SvgEvent.stopPropagationOn "dblclick"
                ]
              )
              [ Svg.rect
                  [ node.x - (size.w / 2) - Styling.darkMode.padding |> String.fromFloat |> SvgAttribute.x
                  , node.y - (size.h / 2) - Styling.darkMode.padding |> String.fromFloat |> SvgAttribute.y
                  , size.w + 2*Styling.darkMode.padding |> String.fromFloat |> SvgAttribute.width
                  , size.h + 2*Styling.darkMode.padding |> String.fromFloat |> SvgAttribute.height
                  , SvgAttribute.rx darkMode.nodeRounding.value
                  , focused id |> Styling.nodeRect Styling.darkMode |> SvgAttribute.css
                  ]
                  []
              , textNode id node
              ]
          NotReady ->
            Svg.g
              [ SvgAttribute.opacity "0.0"
              ]
              [ textNode id node
              ]
      )
  in
    [ svg
      -- VERY IMPORTANT: DO NOT USE `HtmlAttribute.css` here as it will cause
      -- the program to immediately crash when the webpage is loaded
      [ Styling.svg Styling.darkMode |> SvgAttribute.css
      , Background |> Click |> mouseEventDecoder |> SvgEvent.on "mousedown"
      ]
      [ SvgKeyed.node
          "g"
          []
          ( ( model.nodes |> viewConnections )
            ++
            ( model.nodes |> Dict.toList |> List.map viewNode )
          )
      ]
    , Html.form
      [ Styling.form |> HtmlAttribute.css
      , HtmlEvent.onSubmit TextConfirmed
      ]
      [ Html.input
          [ Styling.textField Styling.darkMode |> HtmlAttribute.css
          , HtmlAttribute.spellcheck False
          , HtmlAttribute.autocomplete False
          , HtmlAttribute.value model.textFieldContents
          , HtmlAttribute.id "text-field"
          , HtmlEvent.onInput TextChanged
          ]
          []
      ]
    ]
